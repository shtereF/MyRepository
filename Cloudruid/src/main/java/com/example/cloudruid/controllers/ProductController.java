package com.example.cloudruid.controllers;

import com.example.cloudruid.models.Deal;
import com.example.cloudruid.models.Product;
import com.example.cloudruid.models.ProductDto;
import com.example.cloudruid.services.contracts.DealService;
import com.example.cloudruid.services.contracts.ProductService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    private final DealService dealService;

    @Autowired
    public ProductController(ProductService productService, DealService dealService) {
        this.productService = productService;
        this.dealService = dealService;
    }

    @GetMapping("/{productId}")
    public String getSingleProductPage(@PathVariable int productId, Model model) {
        Product product = productService.findById(productId);
        ProductDto productDto = new ProductDto();
        productDto.setPrice(product.getPrice());
        model.addAttribute("productDto", productDto);
        model.addAttribute("product", product);
        return "product-page";
    }

    @PostMapping("/{productId}")
    public String editProduct(@PathVariable int productId,
                              @Valid @ModelAttribute("productDto") ProductDto productDto) {
        Product product = productService.findById(productId);
        product.setPrice(productDto.getPrice());
        productService.update(product);
        return "redirect:/";
    }

    @GetMapping("/new")
    public String showCreateProductPage(Model model) {
        model.addAttribute("product", new ProductDto());
        return "create-product";
    }

    @PostMapping("/new")
    public String createProduct(@Valid @ModelAttribute("product") ProductDto productDto) {
        Product product = new Product();
        product.setName(productDto.getName());
        product.setPrice(productDto.getPrice());
        productService.create(product);
        return "redirect:/";
    }

    @PostMapping("/{productId}/deals/{dealId}")
    public String addDeal(@PathVariable int productId, @PathVariable int dealId, Model model) {
        Product product = productService.findById(productId);
        Deal deal = dealService.findById(dealId);
        model.addAttribute("deal", deal);
        product.setDeal(deal);
        productService.update(product);
        return "redirect:/";
    }
}
