package com.example.cloudruid.controllers;

import com.example.cloudruid.models.Deal;
import com.example.cloudruid.models.ShoppingCart;
import com.example.cloudruid.services.contracts.DealService;
import com.example.cloudruid.services.contracts.ProductService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class HomePageController {

    private final ProductService productService;

    private final DealService dealService;

    @Autowired
    public HomePageController(ProductService productService, DealService dealService) {
        this.productService = productService;
        this.dealService = dealService;
    }

    @ModelAttribute("shoppingCart")
    public ShoppingCart getShoppingCart(ShoppingCart shoppingCart) {
        return shoppingCart;
    }

    @ModelAttribute("deal")
    public Deal get2for1deal() {
        return dealService.findById(1);
    }

    @GetMapping
    public String showHomePage(Model model) {
        model.addAttribute("products", productService.findAll());
        return "index";
    }

    @PostMapping("/products/{productId}/shoppingCart")
    public String addToShoppingCart(@PathVariable int productId, HttpSession session) {
        ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        if (shoppingCart == null) {
            shoppingCart = new ShoppingCart();
            session.setAttribute("shoppingCart", shoppingCart);
        }
        shoppingCart.getProducts().add(productService.findById(productId));
        shoppingCart.setTotalPrice(productService.calculateTotalPrice(shoppingCart));
        return "redirect:/";
    }

    @GetMapping("/shoppingCart")
    public String showShoppingCart(Model model, HttpSession session) {
        ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        if (shoppingCart == null) {
            shoppingCart = new ShoppingCart();
            session.setAttribute("shoppingCart", shoppingCart);
        }
        model.addAttribute("productsList", shoppingCart.getProducts());
        model.addAttribute("totalPrice", String.format("%.2f",shoppingCart.getTotalPrice()));
        return "shopping-cart";
    }
}
