package com.example.cloudruid.models;


import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private double totalPrice;

    private List<Product> products;

    public ShoppingCart() {
        this.products = new ArrayList<>();
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
