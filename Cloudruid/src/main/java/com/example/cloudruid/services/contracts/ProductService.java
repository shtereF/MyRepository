package com.example.cloudruid.services.contracts;

import com.example.cloudruid.models.Product;
import com.example.cloudruid.models.ShoppingCart;

import java.util.List;

public interface ProductService {

    Product findById(int productId);

    List<Product> findAll();

    void create(Product product);

    void update(Product product);

    void delete(Product product);

    double calculateTotalPrice(ShoppingCart shoppingCart);
}
