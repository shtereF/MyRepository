package com.example.cloudruid.services;

import com.example.cloudruid.models.Deal;
import com.example.cloudruid.repositories.DealRepository;
import com.example.cloudruid.services.contracts.DealService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DealServiceImpl implements DealService {

    private final DealRepository dealRepository;

    @Autowired
    public DealServiceImpl(DealRepository dealRepository) {
        this.dealRepository = dealRepository;
    }

    @Override
    public Deal findById(int dealId) {
        return dealRepository.findById(dealId).orElseThrow(() -> new EntityNotFoundException("Deal not found."));
    }
}
