package com.example.cloudruid.services.contracts;

import com.example.cloudruid.models.Deal;

public interface DealService {

    Deal findById(int dealId);
}
