package com.example.cloudruid.services;

import com.example.cloudruid.models.Product;
import com.example.cloudruid.models.ShoppingCart;
import com.example.cloudruid.repositories.ProductRepository;
import com.example.cloudruid.services.contracts.ProductService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;


    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product findById(int productId) {
        return productRepository.findById(productId).orElseThrow(() -> new EntityNotFoundException("Product not found"));
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public void create(Product product) {
        if (productRepository.existsById(product.getProductId())) {
            throw new EntityExistsException("Product already exists.");
        }
        productRepository.save(product);
    }

    @Override
    public void update(Product product) {
        productRepository.save(product);
    }

    @Override
    public void delete(Product product) {
        if (productRepository.existsById(product.getProductId())) {
            productRepository.delete(product);
        } else {
            throw new EntityNotFoundException("Product not found.");
        }
    }

    @Override
    public double calculateTotalPrice(ShoppingCart shoppingCart) {
        double lowestPrice = shoppingCart.getProducts().get(0).getPrice();
        double totalPrice = 0;
        for (Product product : shoppingCart.getProducts()) {
            totalPrice += product.getPrice();
        }
        boolean isDeal = false;
        if (shoppingCart.getProducts().size() >= 3) {
            for (int i = 0; i < 3; i++) {
                if (shoppingCart.getProducts().get(i).getDeal() != null) {
                    isDeal = true;
                }
                if (shoppingCart.getProducts().get(i).getPrice() <= lowestPrice) {
                    lowestPrice = shoppingCart.getProducts().get(i).getPrice();
                }
            }
            if (!isDeal) {
                totalPrice -= lowestPrice;
            }
        }
        for (int i = 0; i < shoppingCart.getProducts().size() - 1; i++) {
            for (int j = i + 1; j < shoppingCart.getProducts().size(); j++) {
                if (shoppingCart.getProducts().get(i).getDeal() != null) {
                    if (shoppingCart.getProducts().get(i).getName().equals(shoppingCart.getProducts().get(j).getName())
                            && shoppingCart.getProducts().get(i).getDeal().getDealName().equals("buy 1 get 1 half price")) {
                        double discountedPrice = shoppingCart.getProducts().get(j).getPrice() / 2;
                        totalPrice -= discountedPrice;
                        break;
                    }
                }
            }
        }
        return totalPrice;
    }
}
